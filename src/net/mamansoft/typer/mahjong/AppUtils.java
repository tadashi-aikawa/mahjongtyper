package net.mamansoft.typer.mahjong;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.text.ClipboardManager;

public class AppUtils {

    /**
     * get version name<br>
     * return null if aborted
     * 
     * @return version name
     */
    public static String getVersionName(final Context context) {
        final PackageManager manager = context.getPackageManager();
        try {
            final PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
            return info.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * get device model.
     * 
     * @return device model
     */
    public static String getDeviceModel() {
        return Build.DEVICE;
    }

    /**
     * get OS version.
     * 
     * @return OS version
     */
    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * get application name.
     * 
     * @param context
     * @return application name
     */
    public static String getAppName(final Context context) {
        return context.getString(R.string.app_name);
    }

    /**
     * Copy word to Clipboad.<br>
     * This method is {@link Deprecated} in over ApiLv 11.<br>
     * But we use by reason of supporting under ApiLv 11,
     * 
     * @param word
     */
    @Deprecated
    public static void copyToClipboard(final Context context, final String word) {
        final ClipboardManager cm = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
        cm.setText(word);
    }

}
