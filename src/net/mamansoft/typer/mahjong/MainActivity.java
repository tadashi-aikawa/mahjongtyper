package net.mamansoft.typer.mahjong;

import android.os.Bundle;
import android.view.WindowManager.LayoutParams;

import com.actionbarsherlock.app.SherlockFragmentActivity;

/**
 * this is MainActivity<br>
 * you can show when you start application(in real, task).
 * 
 * @author syoumaman
 * 
 */
public class MainActivity extends SherlockFragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // hide keyboard when EditText focused
        getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // must set theme
        setTheme(R.style.Theme_Sherlock);
        setContentView(R.layout.activity_main);

    }
}