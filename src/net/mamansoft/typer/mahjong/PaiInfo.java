package net.mamansoft.typer.mahjong;

/**
 * this class provides you Pai Information<br>
 * 
 * @author syoumaman
 * 
 */
class PaiInfo {
    /**
     * Pai Information Interface<br>
     * the class implements this must have field,
     * imageInformation(ex.imageResId, string ...) and word.
     * 
     * @author syoumaman
     * 
     */
    interface IPaiInfo {
        public int getImageInfo();

        public String getWord();
    }

    /**
     * Pai Crack Information<br>
     * Crack is マンズ
     * 
     * @author syoumaman
     * 
     */
    enum PaiCrackInfo implements IPaiInfo {
        MS1(R.drawable.p_ms1_1, "一"), //
        MS2(R.drawable.p_ms2_1, "二"), //
        MS3(R.drawable.p_ms3_1, "三"), //
        MS4(R.drawable.p_ms4_1, "四"), //
        MS5(R.drawable.p_ms5_1, "五"), //
        MS5R(R.drawable.p_ms5r_1, "ｱｶ五"), //
        MS6(R.drawable.p_ms6_1, "六"), //
        MS7(R.drawable.p_ms7_1, "七"), //
        MS8(R.drawable.p_ms8_1, "八"), //
        MS9(R.drawable.p_ms9_1, "九");

        private int imageResId;
        private String word;

        private PaiCrackInfo(final int imageResId, final String word) {
            this.imageResId = imageResId;
            this.word = word;
        }

        @Override
        public int getImageInfo() {
            return this.imageResId;
        }

        @Override
        public String getWord() {
            return this.word;
        }
    }

    /**
     * Pai Bam Information<br>
     * Bam is ソウズ
     * 
     * @author syoumaman
     * 
     */
    enum PaiBamInfo implements IPaiInfo {
        SS1(R.drawable.p_ss1_1, "１"), //
        SS2(R.drawable.p_ss2_1, "２"), //
        SS3(R.drawable.p_ss3_1, "３"), //
        SS4(R.drawable.p_ss4_1, "４"), //
        SS5(R.drawable.p_ss5_1, "５"), //
        SS5R(R.drawable.p_ss5r_1, "ｱｶ５"), //
        SS6(R.drawable.p_ss6_1, "６"), //
        SS7(R.drawable.p_ss7_1, "７"), //
        SS8(R.drawable.p_ss8_1, "８"), //
        SS9(R.drawable.p_ss9_1, "９");

        private int imageResId;
        private String word;

        private PaiBamInfo(final int imageResId, final String word) {
            this.imageResId = imageResId;
            this.word = word;
        }

        @Override
        public int getImageInfo() {
            return this.imageResId;
        }

        @Override
        public String getWord() {
            return this.word;
        }
    }

    /**
     * Pai Ball Information<br>
     * Ball is ピンズ
     * 
     * @author syoumaman
     * 
     */
    enum PaiBallInfo implements IPaiInfo {
        PS1(R.drawable.p_ps1_1, "①"), //
        PS2(R.drawable.p_ps2_1, "②"), //
        PS3(R.drawable.p_ps3_1, "③"), //
        PS4(R.drawable.p_ps4_1, "④"), //
        PS5(R.drawable.p_ps5_1, "⑤"), //
        PS5R(R.drawable.p_ps5r_1, "ｱｶ⑤"), //
        PS6(R.drawable.p_ps6_1, "⑥"), //
        PS7(R.drawable.p_ps7_1, "⑦"), //
        PS8(R.drawable.p_ps8_1, "⑧"), //
        PS9(R.drawable.p_ps9_1, "⑨");

        private int imageResId;
        private String word;

        private PaiBallInfo(final int imageResId, final String word) {
            this.imageResId = imageResId;
            this.word = word;
        }

        @Override
        public int getImageInfo() {
            return this.imageResId;
        }

        @Override
        public String getWord() {
            return this.word;
        }
    }

    /**
     * Pai HonorTiles Information<br>
     * HonorTiles is 字牌
     * 
     * @author syoumaman
     * 
     */
    enum PaiHonorTilesInfo implements IPaiInfo {
        JS_EAST(R.drawable.p_ji_e_1, "東"), //
        JS_SOUTH(R.drawable.p_ji_s_1, "南"), //
        JS_WEST(R.drawable.p_ji_w_1, "西"), //
        JS_NORTH(R.drawable.p_ji_n_1, "北"), //
        JS_WHITE(R.drawable.p_no_1, "白"), //
        JS_GREEN(R.drawable.p_ji_h_1, "発"), //
        JS_RED(R.drawable.p_ji_c_1, "中");

        private int imageResId;
        private String word;

        private PaiHonorTilesInfo(final int imageResId, final String word) {
            this.imageResId = imageResId;
            this.word = word;
        }

        @Override
        public int getImageInfo() {
            return this.imageResId;
        }

        @Override
        public String getWord() {
            return this.word;
        }
    }
    
    /**
     * Pai String Information<br>
     * It makes button from button-text and word.
     * 
     * @author syoumaman
     * 
     */
    enum PaiStringInfo implements IPaiInfo {
        BTN_HAND(R.string.main_pai_string_image_hand, "手牌"), //
        BTN_SELF_PICK(R.string.main_pai_string_image_self_pick, "ツモ"), //
        BTN_DRAGON(R.string.main_pai_string_image_dragon, "ドラ"), //
        BTN_TURN(R.string.main_pai_string_image_turn, "巡目");

        private int buttonTextResId;
        private String word;

        private PaiStringInfo(final int buttonTextResId, final String word) {
            this.buttonTextResId = buttonTextResId;
            this.word = word;
        }

        @Override
        public int getImageInfo() {
            return this.buttonTextResId;
        }

        @Override
        public String getWord() {
            return this.word;
        }
    }
}
