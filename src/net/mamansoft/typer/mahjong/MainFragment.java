package net.mamansoft.typer.mahjong;

import net.mamansoft.typer.mahjong.PaiInfo.IPaiInfo;
import net.mamansoft.typer.mahjong.PaiInfo.PaiBallInfo;
import net.mamansoft.typer.mahjong.PaiInfo.PaiBamInfo;
import net.mamansoft.typer.mahjong.PaiInfo.PaiCrackInfo;
import net.mamansoft.typer.mahjong.PaiInfo.PaiHonorTilesInfo;
import net.mamansoft.typer.mahjong.PaiInfo.PaiStringInfo;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

/**
 * this Fragment controls MainActivity's layout.<br>
 * for example, Pai, Text, and so on.
 * 
 * 
 * @author syoumaman
 * 
 */
public class MainFragment extends SherlockFragment {

    /** String To Share other applications */
    private EditText mEditText;

    public MainFragment() {
        // empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // TODO: memo
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // view initialize
        final View route = inflater.inflate(R.layout.fragment_main, null);
        mEditText = (EditText) route.findViewById(R.id.main_fragment_text);
        
        final View clearButton = route.findViewById(R.id.main_fragment_clear_image);
        clearButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.getEditableText().clear();
            }
        });
        
        initPaiButton(route);

        return route;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(Menu.NONE, ActionItemInfo.SHARE.id, Menu.NONE, ActionItemInfo.SHARE.title) //
                .setIcon(android.R.drawable.ic_menu_share) //
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(Menu.NONE, ActionItemInfo.COPY.id, Menu.NONE, ActionItemInfo.COPY.title) //
                .setIcon(R.drawable.android_content_copy) //
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        final SubMenu subMenu = menu.addSubMenu(SubMenu.NONE, ActionItemInfo.MENU.id, SubMenu.NONE, ActionItemInfo.MENU.title);
        subMenu.add(Menu.NONE, ActionItemInfo.SUPPORT.id, Menu.NONE, ActionItemInfo.SUPPORT.title);
        subMenu.add(Menu.NONE, ActionItemInfo.MATERIAL.id, Menu.NONE, ActionItemInfo.MATERIAL.title);
        subMenu.add(Menu.NONE, ActionItemInfo.VERSION.id, Menu.NONE, ActionItemInfo.VERSION.title);
        subMenu.getItem() //
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        final ActionItemInfo menuItemType = ActionItemInfo.valueOf(id);

        switch (menuItemType) {
        case SHARE:
            shareWord(mEditText.getText().toString());
            break;
        case COPY:
            AppUtils.copyToClipboard(getActivity(), mEditText.getText().toString());
            Toast.makeText(getActivity(), R.string.main_activity_copy_clipboard_finish, Toast.LENGTH_SHORT).show();
            break;
        case SUPPORT:
            runMailer(createMailSubject(), createMailMessageTemplate());
            break;
        case MATERIAL:
            final String url = getString(R.string.actionbar_main_submenu_material_link_url);
            openUrl(url);
            break;
        case VERSION:
            final String versionName = AppUtils.getVersionName(getActivity());
            Toast.makeText(getActivity(), versionName, Toast.LENGTH_SHORT).show();
            break;
        default:
            break;
        }
        return true;
    }

    // ------------------------------------------------------------------------------------------------------------
    // private method
    // ------------------------------------------------------------------------------------------------------------
    /**
     * Create Pai Image and add to each layout.
     * 
     */
    private void initPaiButton(final View routeView) {
        // Crack Image
        final ViewGroup firstLayout = (ViewGroup) routeView.findViewById(R.id.main_fragment_first_row_layout);
        for (PaiCrackInfo info : PaiCrackInfo.values()) {
            final ImageButton imageView = createPai(info);
            firstLayout.addView(imageView);
        }

        // Bam Image
        final ViewGroup secondLayout = (ViewGroup) routeView.findViewById(R.id.main_fragment_second_row_layout);
        for (PaiBamInfo info : PaiBamInfo.values()) {
            final ImageButton imageView = createPai(info);
            secondLayout.addView(imageView);
        }

        // Ball Image
        final ViewGroup thirdLayout = (ViewGroup) routeView.findViewById(R.id.main_fragment_third_row_layout);
        for (PaiBallInfo info : PaiBallInfo.values()) {
            final ImageButton imageView = createPai(info);
            thirdLayout.addView(imageView);
        }

        // HonorTiles Image
        final ViewGroup fourthLayout = (ViewGroup) routeView.findViewById(R.id.main_fragment_fourth_row_layout);
        for (PaiHonorTilesInfo info : PaiHonorTilesInfo.values()) {
            final ImageButton imageView = createPai(info);
            fourthLayout.addView(imageView);
        }
        
        // String Image
        final ViewGroup fifthLayout = (ViewGroup) routeView.findViewById(R.id.main_fragment_fifth_row_layout);
        for (PaiStringInfo info : PaiStringInfo.values()) {
            final View button = createStringButton(info);
            fifthLayout.addView(button);
        }
    }

    /**
     * share word with other application
     * 
     * @param word
     */
    private void shareWord(final String word) {
        final Intent intent = new Intent() //
                .setAction(Intent.ACTION_SEND) //
                .setType("text/plain") //
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK) //
                .putExtra(Intent.EXTRA_TEXT, word);
        startActivity(intent);
    }

    /**
     * open url page in one's browser
     * 
     * @param url
     */
    private void openUrl(final String url) {
        final Uri uri = Uri.parse(url);
        final Intent intent = new Intent(Intent.ACTION_VIEW, uri) //
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * create subject of mail.
     * 
     * @return subject
     */
    private String createMailSubject() {
        return getString(R.string.common_mail_subject_support, AppUtils.getAppName(getActivity()));
    }

    /**
     * create message template of mail.<br>
     * it includes version name, OS version, model name, and so on.
     * 
     * @return message template
     */
    private String createMailMessageTemplate() {
        final String os = AppUtils.getOSVersion();
        final String model = AppUtils.getDeviceModel();
        final String version = AppUtils.getVersionName(getActivity());
        
        final StringBuilder builder = new StringBuilder() //
                .append(getString(R.string.common_mail_message_support_abstract))
                .append(getString(R.string.common_mail_message_support_block_format, "OS", os)) //
                .append(getString(R.string.common_mail_message_support_block_format, "Model", model)) //
                .append(getString(R.string.common_mail_message_support_block_format, "Version", version));

        return builder.toString();

    }

    /**
     * Run mailer to send mail to support user.
     * 
     * @param subject
     * @param message
     */
    private void runMailer(final String subject, final String message) {
        final Intent intent = new Intent() //
                .setAction(Intent.ACTION_SEND) //
                .setType("message/rfc822") //
                .putExtra(Intent.EXTRA_EMAIL, new String[] { getString(R.string.common_mail_address_support) }) //
                .putExtra(Intent.EXTRA_SUBJECT, subject) //
                .putExtra(Intent.EXTRA_TEXT, message) //
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    /**
     * create ImageView
     * 
     * @param info
     *            Pai Information
     * @return ImageView
     */
    private ImageButton createPai(final IPaiInfo info) {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final ImageButton imageView = (ImageButton) inflater.inflate(R.layout.widget_pai_layout, null);

        imageView.setImageResource(info.getImageInfo());
        imageView.setTag(info.getWord());

        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                insertCurretPos(mEditText, (String) view.getTag());
            }
        });
        return imageView;
    }
    
    /**
     * create StringButton
     * 
     * @param info
     *            Pai Information
     * @return StringButton
     */
    private Button createStringButton(final IPaiInfo info) {
        final Button button = new Button(getActivity());

        button.setText(getString(info.getImageInfo()));
        button.setTag(info.getWord());

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                insertCurretPos(mEditText, (String) view.getTag());
            }
        });
        return button;
    }

    /**
     * Insert a word into an EditText by the press of a button.
     * 
     * @param editText
     * @param word
     */
    private void insertCurretPos(final EditText editText, final String word) {
        final Editable editable = editText.getText();

        final int start = editText.getSelectionStart();
        final int end = editText.getSelectionEnd();

        editable.replace(Math.min(start, end), Math.max(start, end), word);
    }

    /**
     * ActionItem's information.
     * 
     * @author syoumaman
     * 
     */
    private enum ActionItemInfo {
        INIT(""), //
        COPY(""), //
        SHARE(""), //
        MENU("menu"), //
        MATERIAL("素材提供元"), //
        SUPPORT("問い合わせ"), //
        VERSION("バージョン");

        public int id;
        public String title;

        private ActionItemInfo(final String title) {
            this.id = ordinal();
            this.title = title;
        }

        public static ActionItemInfo valueOf(final int id) {
            for (ActionItemInfo itemId : values()) {
                if (itemId.id == id) {
                    return itemId;
                }
            }
            return ActionItemInfo.INIT;
        }
    }

}